import React from 'react'
import { useHistory } from 'react-router-dom'


export const FacebookSharing = () => {

    let path = 'https://funding-project.web.app' + useHistory().location.pathname;

    console.log(path);
    

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    return (
        <>
            <div id="fb-root"></div>
            {/* // facebook sharing button */}
            <div class="fb-share-button" data-href={path}
                data-layout="button_count">
            </div>
        </>
    )
}
