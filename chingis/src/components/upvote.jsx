import React from 'react';
import { useDoc } from '../Hooks/firebase';

export const Upvote = (props) => {
    let { children, disabled, className, upvoted, eventId, ...others } = props;

    let { data } = useDoc(`Events/${eventId}`);
    let { updateRecord } = useDoc(`Events/${eventId}/vote`);

    let upvote = () => {
        updateRecord(data.vote + 1);
    }

    return (
        <button onClick={upvote()} className={`btn vote ${className} ${upvoted && 'upvoted'} ${disabled && 'disabled'}`} {...others}>
            {children}
        </button>
    );
};