import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { AuthContext } from '../providers/auth-user-provider'

export const Verify = () => {

    const history = useHistory();
    const { user } = useContext(AuthContext);

    console.log(user)

    if (user != null) {
        if (user.emailVerified === true) {
            console.log('Verified')
            history.push('/');
        }
    }

    const sendVerification = () => {
        if (user != null) {
            user.sendEmailVerification();
            console.log(user.emailVerified)
        }
    }
    

    const verify = () => {
        sendVerification();
        if (user != null) {
            if (user.emailVerified === true) {
                console.log(user.emailVerified, 'aa')
                history.push('/')
            } else {
                console.log(user.emailVerified)
                window.location.reload(false);
                console.log('aaaa')
            }
        }
    }


    if (user != null) {
        return (
            <div>
                <h1>Check Your Email</h1>
                <button onClick={verify}>Verify</button>
            </div>
        )
    }
    return (
        <div>
            <h3>Loading ...</h3>
        </div>
    )
}