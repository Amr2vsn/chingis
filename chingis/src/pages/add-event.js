import React, { useState, useContext, useRef, useEffect } from 'react';
import { Button, FormInput } from '../components';
import { useCol } from '../Hooks/firebase';
import { AuthContext } from '../providers/auth-user-provider'
import { useFirebase } from '../Hooks/firebase';

export const AddEvent = () => {

    const { user } = useContext(AuthContext)
    const [eventName, setEventName] = useState('');
    const [desc, setDesc] = useState('');
    const [file, setFile] = useState('');
    const [images, setImages] = useState([]);
    const [imageSrc, setImageSrc] = useState('');
    const [srt, setSrt] = useState('');
    const { auth, firebase, firestore } = useFirebase();
    const inputFile = useRef(null);
    const inputSideFile = useRef(null);

    let uid;

    if (user != null) {
        uid = user.uid
    }

    const { createRecord } = useCol(`/users/${uid}/createdEvents/`);
    const { createRecord: createRecord2 } = useCol(`/Events`);
    const { data } = useCol(`/Categories`);


    const handleChangeEventName = (e) => setEventName(e.target.value);
    const handleChangeDesc = (e) => setDesc(e.target.value);

    const randomStringAndNumber = () => {
        let result = '';
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < 7; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    const Add = () => {
        let id = randomStringAndNumber();


        auth.onAuthStateChanged((u) => {
            if (u) {
                console.log(u);
            } else {
                console.log('bhgui')
            }
        });

        var storageRef = firebase.storage().ref().child(`EventImages/${id}/MainImage.jpg`);;
        storageRef.put(file)
            .then((snapshot) => {
                console.log('Done.');
            })
        images.map((e, i) => {
            var storageSideRef = firebase.storage().ref().child(`EventImages/${id}/SideImage-${i}.jpg`);;
            storageSideRef.put(e)
                .then((snapshot) => {
                    console.log('Done.');
                })
            return storageSideRef;
        })

        if (uid) {
            createRecord(id, { name: eventName, desc: desc, id: id, categories: 'test', vote: 0 });
        } else {
            createRecord2(id, { name: eventName, desc: desc, id: id, categories: 'test', vote: 0 });                                                                              
        }


    }


    useEffect(() => {
        if (inputFile || inputSideFile) {

            function onFileChange() {
                setFile(inputFile.current.files[0]);
                setImageSrc(URL.createObjectURL(inputFile.current.files[0]))
                console.log(inputFile.current.files)
            }

            function onSideFileChange() {
                // setFile2(inputSideFile.current.files);
                console.log(inputSideFile.current.files);

                const sideImages = Object.keys(inputSideFile.current.files).filter(key => key !== 'length').map(key => inputSideFile.current.files[key]);
                setImages(sideImages);
            }

            inputFile.current.addEventListener('change', onFileChange);
            inputSideFile.current.addEventListener('change', onSideFileChange);
        }
    }, [inputFile, inputSideFile]);

    useEffect(() => {
        if(firestore) {
            var cat = firestore.collection('Categories');
            cat.orderBy('voted', 'desc').limit(3).onSnapshot((querySnapshot) => {
                setSrt(querySnapshot.docs.map((doc) => doc.data()))
            })
        }
    }, [firestore]);
    
    const cnsole = () =>  {
        srt.map((e) => {
            console.log(e)
        })
    }

    return (
        <div>
            {/* zurag avna */}
            <input type='file' id='file' ref={inputFile} style={{ display: 'none' }} />
            <Button onClick={() => { inputFile.current.click() }}>Upload Event Main Image</Button>

            <img id='img' src={imageSrc} className='w-100 h-100' alt='img'></img>


            {/* </form> */}

            <FormInput placeholder='Event Name' value={eventName} onChange={handleChangeEventName} />

            <FormInput placeholder='Description' value={desc} onChange={handleChangeDesc} />


            {/* add multiple file */}

            <input type='file' id='sidefile' ref={inputSideFile} style={{ display: 'none' }} multiple />
            <Button onClick={() => { inputSideFile.current.click() }}>Upload Event Side Image</Button>

            {
                // console.log(sideImages)
                images && images.map((e, i) => {
                    console.log(e)
                    return (
                        <img src={URL.createObjectURL(e)} key={i} className='w-100 h-100' alt='img'></img>
                    )
                })

            }


            {/* add category */}

            <FormInput placeholder='Categories' />

            {/* Categories */}

            <div>Trending Categories</div>

            <ul>
                {   
                    srt && srt.map((e, i) => {
                        return (
                            <li key={i}>{e.category} voted: {e.voted}</li>
                        )
                    })
                }
            </ul>


            <div>All Categories</div>

            <ul>
                {
                    data.map((e, i) => {
                        return (
                            <li key={i}>{e.category}</li>
                        )
                    })
                }
            </ul>



            <Button onClick={Add}>AddEvent</Button>
            <Button onClick={cnsole}>Console</Button>
        </div>
    )
}