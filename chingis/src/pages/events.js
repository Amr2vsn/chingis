import React, { useState } from 'react'
import { useCol } from '../Hooks/firebase'
import { Button } from '../components/button'

export const ListEvents = () => {
    const { data } = useCol('Events');
    const [state, setState] = useState("Vote")

    // const voted = () => {
    //     if(state === "Vote") {
    //         setState("Voted")
    //     } else {
    //         setState("Vote")
    //     }
    // }

    const handleVote = () => {
        if(state === "Vote") {
            setState("Voted")
        } else {
            setState("Vote")
        }
    }

    return (
        <div className='flex-col'>
            {
                data && (
                    data.map((e) => {
                        return (
                            <div className='flex-center flex-col border-radius-10 mt-10 ml-10'>
                                <div className='flex-col br-black-3 w-300'>
                                    <div className='flex items-center justify-center h-175 w-294'>"PHOTO"</div>
                                    <div className="br-black-1 h-1 w-294"></div>
                                    <div className='flex-center text-center'>
                                        <h2>{e.name}</h2>
                                        <h4>{e.desc}</h4>
                                        <div className="br-black-1 h-1 w-294"></div>
                                        <div className='flex flex-row justify-between items-center'>
                                            <div className="w-147 h-25 text-center">{e.categories}</div>
                                            <div className="br-black-1 h-25 w-1"></div>
                                            <Button className="w-147 h-25" onClick={handleVote}>{state}</Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                )
            }
        </div>
    )
}