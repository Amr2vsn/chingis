const functions = require('firebase-functions');
const admin = require('firebase-admin');


admin.initializeApp();


exports.myfunction = functions.firestore.document("users/{user}/createdEvents/{doc}").onWrite((change, context) => {

    console.log(change.after.data());

    admin.firestore().collection(`Events`).doc(`${change.after.data().id}`).set({
        name: change.after.data().name,
        desc: change.after.data().desc,
        id: change.after.data().id,
        categories: change.after.data().categories,
        vote: 0,
    });

});